# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.4] - 2018-06-25
### Changed
- Project homepage

## [0.1.3] - 2018-06-25
### Changed
- Bugfix

## [0.1.2] - 2018-06-24
### Changed
- Project description
- Formatting
- Bugfix

## [0.1.1] - 2018-06-24
### Changed
- SDK version constraints

## [0.1.0] - 2018-06-24
### Added
- Model class stub
- ModelManager
- Provider widget
- Consumer widget
